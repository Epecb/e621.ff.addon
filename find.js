let backgroundPage = browser.extension.getBackgroundPage();

document.getElementById("find-form").addEventListener("submit", function(e) {
  // Send the query from the form to the background page.
  backgroundPage.find(document.getElementById("find-input").value);
  e.preventDefault();
});

let results = document.getElementById("result-list");

function handleMessage(request, sender, response) {
  // Handle responses coming back from the background page.
  if (request.msg === "clear-results") {
    results.innerHTML = "";
    document.getElementById("action-textarea").value = "";
  }

  if (request.msg === "found-result") {
    // List out responses from the background page as they come in.
    let li = document.createElement("li");
    li.innerText = `Tab url: ${request.url}`;
    results.appendChild(li);
  }

  if (request.msg === "action-result") {
    // List out responses from the background page as they come in.
    document.getElementById("action-textarea").value += `${request.url}`;
  }
};

// select url (Copy to clipboard)
document.getElementById("copyBtn").onclick = function()
{
    document.getElementById('action-textarea').select();
    document.execCommand('copy');
};

// Paste to text area
document.getElementById("pasteBtn").onclick = function()
{
    document.getElementById('action-textarea').select();
    document.execCommand('paste');
};

// Clean text area
document.getElementById("CleanBtn").onclick = function()
{
    document.getElementById("action-textarea").value = "";
};

function onStartedDownload(id) {
  console.log(`Started downloading: ${id}`);
}


function onFailed(error) {
  console.log(`Download failed: ${error}`);
}

// download url
function downtome(url){
    let downloadUrl = url;
    console.log(`${downloadUrl}`);

    rgx =  /\/([^\/]*\.[^.]*$)/;
    fname = rgx.exec(url)[1];
    pth = 'e621.liked/'

    let downloading = browser.downloads.download({
      url : downloadUrl,
      filename : pth + fname ,
      saveAs : false,
      conflictAction : 'uniquify'
      // conflictAction : 'prompt'
    });

    downloading.then(onStartedDownload, onFailed);
}

// download urls
document.getElementById("downloadBtn").onclick = function()
{
    let urls = document.getElementById("action-textarea").value;
    for (let url of urls.split('\n')) {
        if ( url != '' ) {
            downtome(url);
        }
    }
};

browser.runtime.onMessage.addListener(handleMessage);
