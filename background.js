async function find(query) {
  browser.runtime.sendMessage({msg: "clear-results"});

  // If you don't exclude the current tab, every search will find a hit on the
  // current page.
  let this_tab_url = browser.runtime.getURL("find.html");
  let tabs = await browser.tabs.query({});

  for (let tab of tabs) {
    // Iterate through the tabs, but exclude the current tab.
    // TODO[v]: remove - moz-extension:
    if ((tab.url === this_tab_url) || !(/^http.?\:\/\//.test(tab.url)) || !(/^https:\/\/e621.net\/posts\/\d+/.test(tab.url))){
    // if ((tab.url === this_tab_url) || !(/^http.?\:\/\//.test(tab.url))){
      continue;
    }
    browser.runtime.sendMessage({
      msg: "found-result",
      id: tab.id,
      url: tab.url,
    });

    function onExecuted(result) {
        console.log(`We made it green`);
    }

    function onError(error) {
      console.log(`Error: ${error}`);
    }

    var executing = browser.tabs.executeScript(tab.id, {
        file: 'action.js'
    });

    executing.then(onExecuted, onError);
  }
}


browser.browserAction.onClicked.addListener(() => {
  browser.tabs.create({"url": "/find.html"});
});
